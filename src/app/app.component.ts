import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = " Ryukotsei's AWESOME blog!";

  posts = [
      {
        title: "Le Lorem Ipsum, c'est bien.",
        content: "Quanta autem vis amicitiae sit, ex hoc intellegi maxime potest, quod ex infinita societate generis humani, quam conciliavit ipsa natura, ita contracta res est et adducta in angustum ut omnis caritas aut inter duos aut inter paucos iungeretur.",
        loveIts: 0,
        created_at: new Date()
      },
      {
        title: "J'aime beaucoup le Lorem Ipsum",
        content: "C'est vrai quoi, c'est bien pratique quand on veut générer du texte pour nos tests, et qu'on n'a pas la plume de Victor Hugo, ou qu'on n'est pas inspirés. " +
            " Alors un autre petit paragraphe pour la route.Denique Antiochensis ordinis vertices sub uno elogio iussit occidi ideo efferatus, quod ei celebrari vilitatem intempestivam urgenti, cum inpenderet inopia, gravius rationabili responderunt; et perissent ad unum ni comes orientis tunc Honoratus fixa constantia restitisset.",
        loveIts: 600,
        created_at: new Date()
      },
      {
        title: "Et j'aime aussi les films d'animations",
        content: "Absolument aucun rapport! Mais j'avais envie de le souligner. En particulier un faisant partie d'une saga qui s'est terminée avec le dernier sorti en février de cette année.",
        loveIts: 800,
        created_at: new Date()
      },
  ];
}
